#!/usr/bin/env node

import inquirer from 'inquirer';
import handleError from './helpers/handleError.js';
import login from './auth/login.js';
import signup from './auth/signup.js';
import selectOrganisation from './auth/selectOrganisation.js';
import getSDK from './sdk/index.js';
const $sdk = getSDK();


import open, {openApp, apps} from 'open';

console.log(`Let's get started...`)

async function run() {


    const {action} = await inquirer.prompt([{
        type: 'list',
        name: 'action',
        message: 'Hi there, are you a new user?',
        choices:[
        {
            name:'I am new to the Qik platform',
            value:'new',
        },
        {
            name:'I am an existing user',
            value:'existing',
        },

        ],
    }])

    try {
        switch(action) {
            case 'existing':
                 // Login as the user
                await login();
            break;
        case 'new':
                await signup();
            break;
        }

        // Now ask the user to select an organisation
        await selectOrganisation();

        const user = $sdk.auth.getCurrentUser();
        console.log(`You're logged in to '${user.session.organisation.title}'`)
    } catch (e) {
        handleError(e);
    }

    const {token} = $sdk.auth.getCurrentUser();

    console.log('Opening the control dashboard...')
    // Opens the URL in the default browser.

    const url = `https://app.qik.dev/oauth/cli?code=${token.accessToken}`

    console.log('url:', url)
    await open(url);



    const result = await inquirer.prompt([{
        type: 'list',
        name: 'action',
        message: 'What would you like to do now?',
        default:'done',
        choices:[
        {
            name:`I'm done now thanks.`,
            value:'done',
        },
        {
            name:`I'd like to view the documentation`,
            value:'docs',
        },
        {
            name:'I would like to setup a new website or application',
            value:'app',
        },
        ],
    }])

    if(result.action === 'docs') {
    await open(`https://docs.qik.dev`);
    }


}


run();