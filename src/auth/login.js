import inquirer from 'inquirer';

import getSDK from '../sdk/index.js';
const $sdk = getSDK();

import isValidPassword from '../helpers/isValidPassword.js';
import isValidEmailAddress from '../helpers/isValidEmailAddress.js';

export default async function login() {
    const credentials = await inquirer.prompt([{
            type: 'input',
            message: 'Please enter your Qik user email address',
            name: 'email',
            validate(input) {
                return isValidEmailAddress(input);
            },
        },
        {
            type: 'password',
            message: 'Enter your password',
            name: 'password',
            validate(input) {
                return isValidPassword(input);
            },
        },
    ])

    console.log('Checking credentials...');
    const { data } = await $sdk.auth.login(credentials)
    
    if(data.session && data.token) {
    	return {
	        user: data.session,
	        token: data.token,
	    }
    }

    if(data.mfa) {
    	console.log('Multi-factor authentication required.')
	    const { mfa } = await inquirer.prompt([{
	        type: 'password',
	        message: `Please enter your MFA code (We emailed it to '${credentials.email}')`,
	        name: 'mfa',
	    }, ])

	    console.log('Confirming MFA code...');

	    const { data: result } = await $sdk.auth.login({ ...credentials, mfa })
	}
    

    const currentUser = $sdk.auth.getCurrentUser()
    return {
        user: currentUser.session,
        token: currentUser.token,
    }



}