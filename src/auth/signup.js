import inquirer from 'inquirer';
import isValidPassword from '../helpers/isValidPassword.js';
import isValidEmailAddress from '../helpers/isValidEmailAddress.js';
import getSDK from '../sdk/index.js';
const $sdk = getSDK();

export default async function signup() {
    
    const credentials = await inquirer.prompt([
        {
            type: 'input',
            message: 'Please enter your first name',
            name: 'firstName',
            validate(input) {
                return input?.length >= 2
            }
        },
        {
            type: 'input',
            message: 'Please enter your last name',
            name: 'lastName',
            validate(input) {
                return input?.length >= 2
            }
        },
        {
            type: 'input',
            message: 'Please enter your email address',
            name: 'email',
            validate(input) {
                return isValidEmailAddress(input);
            },
        },
        {
            type: 'password',
            message: 'Set a password (minimum 8 characters)',
            name: 'password',
            validate(input) {
                return isValidPassword(input);
            },
        },
        {
            type: 'password',
            message: 'Please enter your password again to confirm',
            name: 'confirmPassword',
        },
    ])

    console.log('Processing...');
    const { data } = await $sdk.auth.signup(credentials)

    if(data.mfa) {
        const { mfa } = await inquirer.prompt([{
            type: 'password',
            message: `Please enter your MFA code (We emailed it to '${credentials.email}')`,
            name: 'mfa',
        }, ])

        console.log('Confirming MFA code...');
        await $sdk.auth.login({ ...credentials, mfa })
    }

    // Get the user as we should now be authenticated
    const user = $sdk.auth.getCurrentUser();

    console.log(`Welcome ${user?.session?.firstName}!`);
    
    return user;






}