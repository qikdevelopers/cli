import inquirer from 'inquirer';
import getSDK from '../sdk/index.js';
import createOrganisation from './createOrganisation.js';

const PUBLIC_ORGANISATION_ID = '62b59cb521fb4772e7b5fa92';
const $sdk = getSDK();



export default async function selectOrganisation() {

	let {session:user, token} = $sdk.auth.getCurrentUser();


    // If the user is in the public organisation
    if(user?.organisation?._id === PUBLIC_ORGANISATION_ID) {
        // Get them to create a new organisation
        console.log('Lets create a new organisation.')

        const org = await createOrganisation();

    }





    const { data: organisations } = await $sdk.api.get(`/user/organisations`)
        
    if (organisations.length <= 1) {
        // The user is only in one organisation
        return;
    }



    const defaultAnswer = {
        name: `${user.organisation.title} (Default)`,
        value: user.organisation._id,
    }

    const choices = [
        defaultAnswer,
        new inquirer.Separator(),
        ...organisations.map(function({ title: name, _id: value }) {
            return {
                name,
                value,
            }
        })
    ]

    const { organisation: organisationID } = await inquirer.prompt([{
        type: 'list',
        name: 'organisation',
        message: 'Please select your organisation',
        choices,
        default: defaultAnswer.value,
    }])

    if (organisationID === defaultAnswer.value) {
        // Do nothing
        console.log('Already in org', user.organisation.title)
    } else {

        console.log(`Selecting organisation...`)
        const result = await $sdk.auth.changeOrganisation(organisationID)
        user = result.session;
        token = result.token;
        console.log(`Organisation selected.`)
    }

     return {user, token};
}