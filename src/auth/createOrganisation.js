import inquirer from 'inquirer';
import getSDK from '../sdk/index.js';
const $sdk = getSDK();

export default async function createOrganisation() {

	let {session:user, token} = $sdk.auth.getCurrentUser();


    let [
        timezones,
        countries,
    ] = await Promise.all([
        $sdk.api.get(`/system/timezones`),
        $sdk.api.get(`/system/countries`),
    ])

    timezones = timezones.data.map(function(timezone) {
        return {
            name:timezone,
            value:timezone,
        }
    });
    countries = countries.data.map(function(country) {
        return {
            name:country.name,
            value:country.alpha2,
        }
    });

        // Get them to register a new organisation;
        const answers = await inquirer.prompt([
        {
            type: 'input',
            name: 'title',
            message: 'What would you like to name your organisation?',
            default:`${user.firstName}'s Organisation`,
        },

        {
            type: 'list',
            name: 'countryCode',
            message: 'Please select your primary country',
            choices: countries,
            default: 'AU',
        },

        {
            type: 'list',
            name: 'timezone',
            message: 'What is your primary timezone?',
            choices: timezones,
            default: 'Australia/Melbourne',
        },

        ])


    const {data} = await $sdk.api.post(`/organisation/register`, answers);



        return answers;


}
