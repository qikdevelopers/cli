

import QikSDK from '@qikdev/sdk';
const $sdk = new QikSDK({
    apiURL: 'production',
});


export default function() {
    return $sdk;
}