export default function isValidPassword(password) {
  if (!password) {
    return false;
  }

  if (password == "password") {
    return false;
  }

  var correctLength = password.length > 7;

  return !!correctLength;
};