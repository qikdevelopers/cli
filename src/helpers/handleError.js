

class ServerError extends Error {
    constructor(statusCode, message, data, type) {
        super(message);
        this.type = type;
        this.errorType = type;
        this.statusCode = statusCode;
        this.data = data;
    }
}

function handleError(obj) {
    if (obj instanceof Error) {
        const message = obj?.response?.data?.message || obj?.response?.data || obj.message || obj;
        const status = obj?.status || obj?.response?.status;
        obj = {
            status,
            message,
        }
    }

    if (obj instanceof ServerError) {
        return obj;
    }

    if (typeof obj == "string") {
        obj = {
            message: obj,
        };
    }

    const err = new ServerError(
        obj.status || 500,
        obj.message || obj,
        obj.data,
        obj.type
    );

    throw err;
};

export default handleError;