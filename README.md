


# Qik CLI

[SDK Documentation](https://sdk.docs.qik.dev) | [REST API Documentation](https://rest.docs.qik.dev) | [UI Kit Documentation](https://ui.docs.qik.dev) | [Website](https://qik.dev) 


## Installation

```bash
npm install @qikdev/cli --save
```

## Getting Started
